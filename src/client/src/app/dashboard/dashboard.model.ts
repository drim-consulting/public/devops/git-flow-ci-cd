export interface Measurement {
  id: number;
  temperatureInCelsius: number;
  humidityPercent: number;
  atmospherePressure: number;
}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Measurement } from './dashboard.model';

@Component({
  selector: 'mc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  cards = [{ title: 'Environment conditions', cols: 2, rows: 2 }];
  measurements: Measurement[] = [];

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.httpClient
      .get<Measurement[]>('/api/measurements')
      .subscribe(result => (this.measurements = result));
  }
}

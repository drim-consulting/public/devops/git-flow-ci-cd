﻿using AutoMapper;
using MonitoringCenter.Domain;
using MonitoringCenter.WebApi.Dtos;

namespace MonitoringCenter.WebApi.Mapping
{
    public class MeasurementProfile : Profile
    {
        public MeasurementProfile()
        {
            CreateMap<Measurement, MeasurementDto>();
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MonitoringCenter.DataAccess;
using MonitoringCenter.WebApi.Dtos;

namespace MonitoringCenter.WebApi
{
    [Route("/api/measurements")]
    public class MeasurementsController : Controller
    {
        private readonly MonitoringCenterDbContext _db;

        private readonly IMapper _mapper;

        public MeasurementsController(
            MonitoringCenterDbContext db,
            IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> Index()
        {
            var measurements = await _db.Measurements.ToListAsync();

            var measurementDtos = _mapper.Map<IEnumerable<MeasurementDto>>(measurements);

            return Json(measurementDtos);
        }
    }
}
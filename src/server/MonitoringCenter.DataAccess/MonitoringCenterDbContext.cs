﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using MonitoringCenter.Domain;

namespace MonitoringCenter.DataAccess
{
    public class MonitoringCenterDbContext : DbContext
    {
        public DbSet<Measurement> Measurements { get; set; }

        public MonitoringCenterDbContext(DbContextOptions<MonitoringCenterDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.ConfigureWarnings(builder =>
            {
                builder.Throw(RelationalEventId.QueryClientEvaluationWarning);
            });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Measurement>().HasData(
                new Measurement { Id = 1, TemperatureInCelsius = 22.1, HumidityPercent = 85.3, AtmospherePressure = 741 },
                new Measurement { Id = 2, TemperatureInCelsius = 20.0, HumidityPercent = 79.4, AtmospherePressure = 745 },
                new Measurement { Id = 3, TemperatureInCelsius = 21.9, HumidityPercent = 77.1, AtmospherePressure = 740 }
            );
        }
    }
}
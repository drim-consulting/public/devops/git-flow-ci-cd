﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MonitoringCenter.Configuration;

namespace MonitoringCenter.DataAccess
{
    public class DesignTimeMonitoringCenterContextFactory : IDesignTimeDbContextFactory<MonitoringCenterDbContext>
    {
        public MonitoringCenterDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            
            var connectionString = configuration[KeyFor.PostgreSql.ConnectionString];

            var builder = new DbContextOptionsBuilder<MonitoringCenterDbContext>();

            builder.UseNpgsql(connectionString);

            return new MonitoringCenterDbContext(builder.Options);
        }
    }
}
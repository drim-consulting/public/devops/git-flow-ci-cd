﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace MonitoringCenter.DataAccess.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Measurements",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TemperatureInCelsius = table.Column<double>(nullable: false),
                    HumidityPercent = table.Column<double>(nullable: false),
                    AtmospherePressure = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurements", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Measurements",
                columns: new[] { "Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius" },
                values: new object[] { 1L, 741.0, 85.3, 22.1 });

            migrationBuilder.InsertData(
                table: "Measurements",
                columns: new[] { "Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius" },
                values: new object[] { 2L, 745.0, 79.4, 20.0 });

            migrationBuilder.InsertData(
                table: "Measurements",
                columns: new[] { "Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius" },
                values: new object[] { 3L, 740.0, 77.1, 21.9 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Measurements");
        }
    }
}

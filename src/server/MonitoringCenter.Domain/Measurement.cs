﻿namespace MonitoringCenter.Domain
{
    public class Measurement
    {
        public long Id { get; set; }

        public double TemperatureInCelsius { get; set; }

        public double HumidityPercent { get; set; }

        public double AtmospherePressure { get; set; }
    }
}
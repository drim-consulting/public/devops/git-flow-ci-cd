SERVER_BUILD_IMAGE_TAG=$CI_REGISTRY_IMAGE/web-api-build:$IMAGE_VERSION
WEB_API_DIST_IMAGE_TAG=$CI_REGISTRY_IMAGE/web-api:$IMAGE_VERSION
MIGRATION_DIST_IMAGE_TAG=$CI_REGISTRY_IMAGE/migration:$IMAGE_VERSION
WEB_APP_DIST_IMAGE_TAG=$CI_REGISTRY_IMAGE/web-app:$IMAGE_VERSION

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker build -t $SERVER_BUILD_IMAGE_TAG -f ./src/server/Dockerfile.build ./src/server
docker create --name server-dist-$IMAGE_VERSION $SERVER_BUILD_IMAGE_TAG
docker cp server-dist-$IMAGE_VERSION:/app/MonitoringCenter.WebApi/bin/Release/netcoreapp2.0/publish/ ./src/server/MonitoringCenter.WebApi/app
docker rm -f server-dist-$IMAGE_VERSION

docker build -t $WEB_API_DIST_IMAGE_TAG -f ./src/server/MonitoringCenter.WebApi/Dockerfile.dist ./src/server/MonitoringCenter.WebApi
docker build -t $MIGRATION_DIST_IMAGE_TAG -f ./src/server/Database/Dockerfile.dist ./src/server/Database

docker build -t $WEB_APP_DIST_IMAGE_TAG -f ./src/client/Dockerfile.build ./src/client

docker push $WEB_API_DIST_IMAGE_TAG
docker push $MIGRATION_DIST_IMAGE_TAG
docker push $WEB_APP_DIST_IMAGE_TAG